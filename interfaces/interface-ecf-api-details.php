<?php
interface ECF_API_Details {
	const APIkey = 'licensekey';
	const ResourseToken = 'check_token';
	const QueryPath = 'modules/servers/licensing/verify.php';
	const ConnectDate = 'checkdate';
	const APIConnection = 'remotecheck';
}