<?php
/* An abstract class for providing integeral options */
abstract class ECF_Option_Dimensions extends ECF_Option {

	public function horizontal_label() {
		return 'Width';
	}

	public function vertical_label() {
		return 'Height';
	}

	public function options_form_field( $ref, $field = null ) {
		extract( $this->get_value( $field ) );
		return sprintf( '<fieldset><label>%s <input type="number" '
			. 'class="small-text" min="0" step="1" name="%s_w[%s]" '
			. 'value="%s" /></label> '
			. '<label>%s <input type="number" '
			. 'class="small-text" min="0" step="1" name="%s_h[%s]" '
			. 'value="%s" /></label></fieldset>',
			$this->horizontal_label(), $this->get_name(), $ref, $w,
			$this->vertical_label(), $this->get_name(), $ref, $h );
	}

	public function options_form_field_placeholder( $field = null ) {
		extract( $this->get_value( $field ) );
		return sprintf( '<fieldset><label>%s <input type="number" '
			. 'class="small-text" min="0" step="1" '
			. 'value="%s" disabled="disabled" /></label> '
			. '<label>%s <input type="number" '
			. 'class="small-text" min="0" step="1" '
			. 'value="%s" disabled="disabled" /></label></fieldset>',
			$this->horizontal_label(), $w, $this->vertical_label(), $h );
	}

	public function options_form_post_single( $ref, $field ) {
		$name = $this->get_name();
		$value = array();

		foreach (array( 'w', 'h' ) as $dim) {
			if ( isset( $_POST["${name}_${dim}"][$ref] ) )
				$value[$dim] = $_POST["${name}_${dim}"][$ref];
		}

		$this->update_value( $field, $value );
	}

	public function update_value( $field, $value ) {
		$values = array();
		foreach (array( 'w', 'h' ) as $dim) {
			$values[$dim] = isset( $value[$dim] ) ? intval($value[$dim]) : '';
		}
		parent::update_value( $field, vsprintf('w: %s, h: %s', $values) );
	}

	public function get_value( $field ) {
		$raw_value = parent::get_value( $field );
		$default = $this->get_default_value();
		$value = array();

		foreach (array( 'w', 'h' ) as $dim) {
			preg_match( "/$dim: ([0-9]+)/i", $raw_value, $matches );
			$value[$dim] = isset( $matches[1] )
				? intval( $matches[1] ) : $default[$dim];
		}

		return $value;
	}

	public function sql_definition() {
		return sprintf( '`%s` varchar(255) NOT NULL DEFAULT \'\'',
			$this->get_name(), $this->get_default_value() );
	}
}
