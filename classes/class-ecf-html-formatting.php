<?php
class ECF_HTML_Formatting {

	public static function image( $url, $class, $w = 'null', $h = 'null',
		$caption = '' ) {
		return sprintf( '<figure class="%s"><div class="ecf-response" '
			. '<a href="%s"><img src="%s" class="ecf-image" alt="%s" '
			. '/></a></div><figcaption class="ecf-question">%s</figcaption> '
			. '</figure>',
			$class, $url, self::thumbnail( $url, $w, $h ), $caption,
			$caption );
	}

	public static function thumbnail( $url, $w='null', $h='null', $zc=3 ) {
		$url = urlencode( $url );
		return plugins_url( ECF_LIB_PATH . '/timthumb.php', ECF_ROOT_FILE )
			. "?src=$url&zc=$zc&w=$w&h=$h";
	}

	public static function selected( $choice, $chosen = true ) {
		return ( $choice === $chosen ) ? 'selected' : '';
	}

	public static function checked( $choice, $chosen = true ) {
		return ( $choice === $chosen ) ? 'checked="checked"' : '';
	}

	public static function html_string( $string ) {
		return htmlspecialchars( $string, 32, 'UTF-8' );
	}

	public static function list_classes( $class = null, $classes = null ) {
		return implode( ' ', array_merge( (array)$class, (array)$classes ) );
	}

	public static function image_upload_field( $id, $class ) {
		return sprintf( '<input type="file" id="%s" name="%s" class="%s" />',
			$id, $id, $class );
	}

	public static function checkbox_field( $id, $checked, $class,
		$value = 1 ) {
		return sprintf( '<input type="checkbox" id="%s" name="%s" class="%s" '
			. 'value="%s" %s />', $id, $id, $class, $value,
			self::checked( (bool)$checked ) );
	}

	public static function select_field( $id, $choices, $chosen, $class ) {
		assert(array_key_exists($chosen, $choices));
		$html = '';
		$html .= sprintf( "<select id=\"%s\" name=\"%s\" class=\"%s\">\n",
			$id, $id, $class );
		foreach ( $choices as $choice => $desc ) {
			$html .= sprintf( "\t<option value=\"%s\" %s>%s</option>\n",
				self::html_string( $choice ),
				self::selected( $choice, $chosen ),
				self::html_string( $desc ) );
		}
		$html .= "</select>\n";
		return $html;
	}

	public static function radio_field( $id, $choices, $chosen, $class ) {
		assert(array_key_exists($chosen, $choices));
		$html = '';
		$html .= sprintf( "<fieldset id=\"%s\" class=\"%s\">\n", $id, $class );
		foreach ( $choices as $choice => $desc ) {
			$html .= sprintf( "\t<p><label for=\"%s\"><input type=\"radio\" "
				. "id=\"%s\" name=\"%s\" value=\"%s\" %s/>\n%s</label></p>\n",
				"$id-$choice", "$id-$choice", $id,
				self::html_string( $choice ),
				self::checked( $choice, $chosen ),
				self::html_string( $desc ) );
		}
		$html .= "</fieldset>\n";
		return $html;
	}

	public static function checkboxes_field( $id, $choices, $chosen, $class ) {
		assert(array_key_exists($chosen, $choices));
		$html = '';
		$html .= sprintf( "<fieldset id=\"%s\" class=\"%s\">\n", $id, $class );
		foreach ( $choices as $choice => $desc ) {
			$html .= sprintf( "\t<p><label for=\"%s\"><input "
				. "type=\"checkbox\" id=\"%s\" name=\"%s[]\" value=\"%s\" "
				. "%s/>\n%s</label></p>\n", "$id-$choice", "$id-$choice", $id,
				self::html_string( $choice ),
				checked( in_array( $choice, $chosen ) ), $desc );
		}
		$html .= "</fieldset>\n";
		return $html;
	}
}
