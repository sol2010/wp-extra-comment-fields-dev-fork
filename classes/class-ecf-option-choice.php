<?php
abstract class ECF_Option_Choice extends ECF_Option {

	abstract public function get_choices();

	public function get_choice_description( $key ) {
		$choices = $this->get_choices();
	}

	public function get_value( $field ) {
		global $ecfdb;

		$value = parent::get_value( $field );

		return array_key_exists( $value, $this->get_choices() )
			? $ecfdb->html_string( $value ) : $this->get_default_value();
	}
}