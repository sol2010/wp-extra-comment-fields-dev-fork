<?php
require_once ECF_CLASSES_DIR . '/class-ecf-option-choice.php';

/* An abstract class for providing selection selection based options */
abstract class ECF_Option_Select extends ECF_Option_Choice {

	public function options_form_field( $ref, $field = null ) {
		return ECF_HTML_Formatting::select_field(
			$this->get_field_name( $ref ), $this->get_choices(),
			$this->get_value( $field ), 'ecf-radio-option' );
	}
}