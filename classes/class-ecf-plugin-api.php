<?php
require_once ECF_CLASSES_DIR . '/class-ecf-feature-pointer.php';
require_once ECF_INTERFACES_DIR . '/interface-ecf-api-details.php';

class ECF_Plugin_API implements ECF_API_Details {
	const LOCAL_API_OPTION_NAME = 'ecf_members_local_key';
	const VERSION_API_OPTION_NAME = 'ecf_members_plugin_api_key';
	const UPDATE_NOTIFICATION_VERSION_ID = 'ecf_update_notification_version_ID';
	const SETTINGS_SECTION = 'ecf_settings_members';
	private $members_url = 'https://solaceten.info/clientarea/';
	private $prs_server = 'http://www.solaceten.info/wp-admin/admin-ajax.php';
	private $members_support_key = 'G1GTQUqAZiMQxARDs17j2KOzP79kAZg5AXX0xTl1PkSzft4XGwatBdY9PGOiK75';
	private $notification_id_version_period = 3;
	private $open_support_time = 3;
	private $update_days = 1;


	public function __construct() {
		add_action( 'admin_init',
			array( &$this, 'register_settings_pluginapi' ) );
		add_action( 'admin_head', array( &$this, 'admin_member_update' ) );

		// Add Plugin API Feature Pointer
		$links = ecf_plugin_data( 'links' );
		new ECF_Feature_Pointer( 'ecf-plugin-api-key',
			'#' . self::VERSION_API_OPTION_NAME,
			'Enter your Plugin API Key',
			sprintf( '<p>Please enter your Plugin API Key (<a href="%s" '
				. 'target="_blank">available from the Client Area</a>) in '
				. 'order to start adding Extra Comment Fields.',
				$links['Client Area'] ),
			$edge = 'top', $align = 'bottom' );
	}


	public function admin_member_update() {
		if ( '' === $this->update_plugin_construct() ) {
			$msg = sprintf( 'Thank you for purchasing %s. Please enter '
				. 'your Plugin API Key. You can retrieve this from your '
				. 'client account page on our website.<br />'
				. '<em><a href="options-general.php?page=%s">Enter Plugin '
				. 'API key here</a></em>',
				ECF_Main::PLUGIN_NAME, ECF_Admin::GLOBAL_SETTINGS_PAGE );
			ECF_Admin_Notifications::message_updated( $msg );
		} else {
			$update_enabled = $this->update_core();

			$update_msg_admin = null;

			switch ( $update_enabled['status'] ) {
			case 'Active':
				break;
			case 'Invalid':
				$update_msg_admin = sprintf( '%s has detected a problem! Please contact support quoting error code:'
					. 'X001. (%s).',
					ECF_Main::PLUGIN_NAME, $update_enabled['description'] );
				break;
			case 'Expired':
				$update_msg_admin = sprintf( '%s has detected a problem! Please contact support quoting error code:'
					. 'X002.' );
				break;

			default:
				$update_msg_admin = sprintf( '%s has detected a problem! Please contact support quoting error code: '
					. 'X003', ECF_Main::PLUGIN_NAME );
				break;
			}

			if ( ! is_null( $update_msg_admin ) )
				ECF_Admin_Notifications::message_alert( $update_msg_admin );
		}
	}


	public function get_deliverables() {
		$update_enabled = $this->update_core();
		$on_localhost = preg_match( '/^http:\/\/(localhost|127.0.0.1)\//',
			get_site_url() );

		return 'Active' === $update_enabled['status']
			|| $on_localhost;
	}


	public function register_settings_pluginapi() {

		add_settings_section( self::SETTINGS_SECTION,
			'Plugin API Details',
			array( &$this, 'settings_licensing_description' ),
			ECF_Admin::GLOBAL_SETTINGS_PAGE );


		add_settings_field( self::VERSION_API_OPTION_NAME,
			'Plugin API Key', array( &$this, 'settings_field_plugin_API' ),
			ECF_Admin::GLOBAL_SETTINGS_PAGE, self::SETTINGS_SECTION );
	}


	public function settings_licensing_description() {
?>
<p>Your Membership Details. <strong><em>Please enter your plugin API key below (sent to you in the welcome email).</em></strong></p>
<?php
	}


	public function settings_field_plugin_API() {
		$name = sprintf( '%s[%s]', ECF_Admin::GLOBAL_SETTINGS_PAGE,
			self::VERSION_API_OPTION_NAME );
?>
<input name="<?php echo $name; ?>" type="text" class="regular-text"
	id="<?php echo self::VERSION_API_OPTION_NAME; ?>"
	value="<?php echo $this->update_plugin_construct(); ?>" />
<?php
	}

	private function set_distribution_package( $releaseID ) {
		set_transient( self::LOCAL_API_OPTION_NAME, $releaseID,
			( $this->open_support_time + $this->open_support_enabled )*24*3600 );
	}

	public function get_distribution_package() {
		return get_transient( self::LOCAL_API_OPTION_NAME );
	}

	public function set_plugin_API_ID( $releaseID ) {
		$settings_page_options = get_option( ECF_Admin::GLOBAL_SETTINGS_PAGE );
		$settings_page_options[self::VERSION_API_OPTION_NAME] = $releaseID;
		update_option( ECF_Admin::GLOBAL_SETTINGS_PAGE,
			$settings_page_options );
	}

	private function update_plugin_construct() {
		$settings_page_options = get_option( ECF_Admin::GLOBAL_SETTINGS_PAGE );
		return isset( $settings_page_options[self::VERSION_API_OPTION_NAME] )
			? $settings_page_options[self::VERSION_API_OPTION_NAME] : '';
	}

	private function query( $form_url, $post_fields ) {
		$headers = array(
			'Content-type' => 'application/x-www-form-urlencoded'
		);
		$response = wp_remote_post( $form_url, array(
				'method' => 'POST',
				'timeout' => 30,
				'redirection' => 3,
				'httpversion' => '1.0',
				'blocking' => true,
				'headers' => $headers,
				'body' => http_build_query( $post_fields )
			) );

		return wp_remote_retrieve_body( $response );
	}

	public function get_update_notification_version_id() {
		$cached_version = get_transient( self::UPDATE_NOTIFICATION_VERSION_ID );
		if ( ! is_null( $cached_version ) )
			return $cached_version;

		$data = json_decode( $this->query( $this->prs_server, array(
					'action' => 'plugin_version',
					'plugin_id' => 'ecf'
				) ) );
		set_transient( self::UPDATE_NOTIFICATION_VERSION_ID, $data->plugin_version,
			$this->notification_id_version_period * 24 * 3600 );
		return $data->plugin_version;
	}

	public static function is_version_newer( $version ) {
		return ( 100 * ECF_VERSION ) < ( 100 * $version );
	}


	public function update_core() {
		$update_enabled = null;
		try {
			$update_enabled = $this->update_core_raw();
		} catch ( Exception $e ) {
			$update_enabled = array( 'status' => 'Error' );
		}

		if ( 'Enabled' === $update_enabled['status']
			&& in_array( 'localpluginAPI', $update_enabled ) )
			$this->set_distribution_package( $update_enabled['localpluginAPI'] );

		return $update_enabled;
	}


	private function update_core_raw() {
		$plugin_API_ID2 = $this->update_plugin_construct();
		$localpluginAPI = $this->get_distribution_package();
		$membersurl = $this->members_url;
		$members_support_key = $this->members_support_key;
		$request_token = time().md5( mt_rand( 1000000000, 9999999999 ).$plugin_API_ID2 );
		$connect_date = date( "Ymd" );
		$usersip = isset( $_SERVER['SERVER_ADDR'] ) ? $_SERVER['SERVER_ADDR'] : $_SERVER['LOCAL_ADDR'];
		$mem_Ylong = $this->open_support_time;
		$package_update_ready = $this->open_support_enabled;
		$lossy_uptime = false;
		$results = null;
		if ( $localpluginAPI ) {
			$localpluginAPI = str_replace( "\n", '', $localpluginAPI );
			$localdata = substr( $localpluginAPI, 0, strlen( $localpluginAPI )-32 );
			$md5hash = substr( $localpluginAPI, strlen( $localpluginAPI )-32 );
			if ( $md5hash==md5( $localdata.$members_support_key ) ) {
				$localdata = strrev( $localdata );
				$md5hash = substr( $localdata, 0, 32 );
				$localdata = substr( $localdata, 32 );
				$localdata = base64_decode( $localdata );
				$lossy_uptime_results = unserialize( $localdata );
				$original_lossy_date = $lossy_uptime_results[self::ConnectDate];
				if ( $md5hash==md5( $original_lossy_date.$members_support_key ) ) {
					$localexpiry = date( "Ymd", mktime( 0, 0, 0, date( "m" ), date( "d" )-$mem_Ylong, date( "Y" ) ) );
					if ( $original_lossy_date>$localexpiry ) {
						$lossy_uptime = true;
						$results = $lossy_uptime_results;
						$compress_file_url = explode( ",", $results["domain"] );
						if ( !in_array( $_SERVER['SERVER_NAME'], $compress_file_url ) ) {
							$lossy_uptime = false;
							$lossy_uptime_results["status"] = "Current_Box";
							$results = array();
						}
						if ( ! preg_match( '/^wp\-extra\-comment\-fields/',
								$results['productname'] ) ) {
							$lossy_uptime = false;
							$lossy_uptime_results['status'] = 'Current_Box';
							$results = array();
						}
					}
				}
			}
		}


		if ( !$lossy_uptime ) {
			$postfields[self::APIkey] = $plugin_API_ID2;
			$postfields["domain"] = $_SERVER['SERVER_NAME'];
			$postfields["ip"] = '';
			$postfields["dir"] = '';
			if ( $request_token ) $postfields[self::ResourseToken] = $request_token;

			$query_url = $membersurl . self::QueryPath;

			$data = $this->query( $query_url, $postfields );

			if ( !$data ) {
				$localexpiry = date( "Ymd", mktime( 0, 0, 0, date( "m" ), date( "d" )-( $mem_Ylong+$package_update_ready ), date( "Y" ) ) );
				if ( $original_lossy_date>$localexpiry ) {
					$results = $lossy_uptime_results;
				} else {
					$results["status"] = "Current_Box";
					$results["description"] = "API connection failed";
					return $results;
				}
			} else {
				preg_match_all( '/<(.*?)>([^<]+)<\/\\1>/i', $data, $matches );
				$results = array();
				foreach ( $matches[1] as $k=>$v ) {
					$results[$v] = $matches[2][$k];
				}
			}
			if ( $results["md5hash"] ) {
				if ( $results["md5hash"]!=md5( $members_support_key.$request_token ) ) {
					$results["status"] = "Current_Box";
					$results["description"] = "API Data Corrupt";
					return $results;
				}
			}
			if ( isset( $results['productname'] ) &&
				!preg_match( '/^wp\-extra\-comment\-fields/',
					$results['productname'] ) ) {
				$results['status'] = 'Current_Box';
				$results['description'] = sprintf( 'Oops - Your API key is missing  '
					. 'for the %s plugin', ECF_Main::PLUGIN_NAME );
			}
			if ( $results["status"]=="Active" ) {
				$results[self::ConnectDate] = $connect_date;
				$data_encoded = serialize( $results );
				$data_encoded = base64_encode( $data_encoded );
				$data_encoded = md5( $connect_date.$members_support_key ).$data_encoded;
				$data_encoded = strrev( $data_encoded );
				$data_encoded = $data_encoded.md5( $data_encoded.$members_support_key );
				$data_encoded = wordwrap( $data_encoded, 80, "\n", true );
				$results["localkey"] = $data_encoded;
			}
			$results[self::APIConnection] = true;
		}
		return $results;
	}
}
