## Version 2.24 ##

$ = jQuery

sidepanes = -> $('.ecf-comment-sidepane')
sidepaneimages = ->
	left: $('.ecf-image-position-left-center')
	right: $('.ecf-image-position-right-center')
repositionedimages = -> $('.ecf-image-position:not(.ecf-image-position-inline-with-comments)')

$ ->
	addimagefallbacks()
	positionform()
	setcommentformenctype()
	reparentelements repositionedimages()
	spimages = sidepaneimages()
	unless $.isEmptyObject spimages
		createsidepane spimages
		sps = sidepanes()
		ifiebelow 8, -> sps.css height: 'auto'
		adjustsidepanes sps, spimages
		positionimageforoldie spimages
		ifnotiebelow 8, ->
			$(window).resize -> adjustsidepanes sps, spimages
			$(window).delay(200).queue -> $(window).resize()
		# Who knows why this is even necessary?
		ifiebelow 10, ->
			resizetoparents sps
			resizetoparents sps

adjustsidepanes = (sidepanes, images) ->
	resizetoparents sidepanes

positionimageforoldie = (images) ->
	ifiebelow 8, ->
		bothsides(images).each ->
			$(@).css 'margin-top': ($(@).parent().height() - $(@).height()) / 2

bothsides = (images) -> images.left.add(images.right)

isiebelow = (version) ->
	$.browser.msie && parseInt($.browser.version, 10) < version

ifiebelow = (version, f) ->
	f() if isiebelow version

ifnotiebelow = (version, f) ->
	f() unless isiebelow version

resizetoparents = (elems) ->
	elems.each ->
		$(@).css 'height': $(@).parent().outerHeight()

reparentelements = (images) ->
	images.each ->
		$(@).parent().prepend $(@)

commentsidepane = (type) ->
	"<div class=\"ecf-comment-sidepane ecf-comment-sidepane-#{type}\" />"

createsidepane = (images) ->
	images.left.wrap commentsidepane 'left'
	images.right.wrap commentsidepane 'right'

findcommentform = -> $ 'form[id^=comment]'

formparts = (commentform, fields = '#ecf_form_fields') ->
	fields: toplevel commentform.find fields
	comment: toplevel commentform.find 'textarea[name=comment]'
	submit: toplevel commentform.find ':submit'

arrangeform = (parts, position = 'above') ->
	switch position
		when 'above' then [parts.fields, parts.comment, parts.submit]
		when 'below' then [parts.comment, parts.fields, parts.submit]

positionform = (position = 'above', form = '#ecf_form_fields') ->
	commentform = findcommentform()
	elems = arrangeform formparts(commentform), position
	commentform?.append elem for elem in elems when elem?

toplevel = (elem, i = 10) ->
	if elem.parent().prop('nodeName') is 'FORM'
		elem
	else if i > 0
		toplevel elem.parent(), i - 1

setcommentformenctype = (enctype = 'multipart/form-data') ->
	findcommentform()?.attr 'enctype': enctype

addimagefallbacks = ->
	$('img.ecf-image').each (i) ->
		$(@).error ->
			fallback = decodeURIComponent urlparam $(@).attr('src'), 'src'
			$(@).attr 'src', fallback
			$(@).parent().attr 'href', fallback
			$(@).unbind 'error'

urlparam = (url, name) ->
	regex = /&?([a-z]+)=([^&]+)/g
	while (match = regex.exec url)? and match?[1] isnt 'src' then
	match?[2]

imageerror = (elem, imageurl) ->
	elem.attr('src', imageurl)
