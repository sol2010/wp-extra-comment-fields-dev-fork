{spawn} = require 'child_process'

### Configuration ###

sources =
	js: ['./js/script']
	stylus: ['./css/admin.styl', './css/ecf.styl']

opts =
	coffee: []
	coffeedebug: ['-b']
	closure: []
	stylus: ['--compress']
	tests: ['--use-color']

### Main Tasks ###

task 'sbuild', 'Build from within Sublime Text', ->
	invoke 'build'

task 'build', 'Build everything from source', ->
	invoke 'js'
	invoke 'css'

### CoffeeScript / JavaScript ###

task 'js', 'Compile and minify CoffeeScript / JavaScript', ->
	invoke 'coffee'
	invoke 'minify-js'

task 'readable-js', 'Compile human readable JavaScript', ->
	for source in sources.js
		run 'coffee', [opts.coffeedebug..., '-c', "#{source}.coffee"], ->
			run 'cp', ["#{source}.js", "#{source}.min.js"]

task 'coffee', 'Complile the CoffeeScript files to JavaScript', ->
	for source in sources.js
		run 'coffee', [opts.coffee..., '-c', "#{source}.coffee"]

task 'minify-js', 'Tell Google Closure Compiler to minify the JavaScript', ->
	for source in sources.js
		run 'java', ['-jar', 'compiler.jar', opts.closure...,\
			'--js', "#{source}.js", '--js_output_file', "#{source}.min.js"]

### Stylus / CSS ###

task 'css', 'Compile and minify Stylus / CSS', ->
	invoke 'stylus'

task 'stylus', 'Compile stylus to CSS', ->
	run 'stylus', [opts.stylus..., sources.stylus...]

### Tests ###

task 'quicktest', 'Test in priority mode', ->
	run 'ruby', ['tests/test-all.rb', '--priority-mode', opts.tests...]

task 'test', 'Run the full test suite', ->
	run 'ruby', ['tests/test-all.rb', opts.tests...]

task 'test-admin', 'Run the admin test suite', ->
	run 'ruby', ['tests/test-admin.rb', opts.tests...]

task 'test-comment-form', 'Run the comment form test suite', ->
	run 'ruby', ['tests/test-comment-form.rb', opts.tests...]

### Functions ###

run = (cmd, args, callback = undefined) ->
	proc = spawn cmd, args
	proc.stderr.pipe process.stderr, end: false
	proc.stdout.pipe process.stdout, end: false
	proc.on 'exit', (code) ->
		callback?() if code is 0
