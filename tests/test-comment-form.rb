require 'watir-webdriver'
require 'test-unit'
require_relative 'testing'

class TestCommentForm < Test::Unit::TestCase

	extend Testing

	class << self
		def startup
			@@config = load_config

			@@browser = @@config['browser']
			@@flood_protection = @@config['flood_protection']
			@@client ||= Watir::Browser.new @@browser
		end

		def shutdown
			@@client.close
		end
	end

	def setup
		@comment_form_url = @@config['site_url'] + '?p=1#comments'
		@@client.goto @comment_form_url
	end

	def teardown
		sleep 12 if @@flood_protection
	end

	def test_adding_comment
		# Fill out comment form
		insert_basic_details

		# Submit and verify results
		submit_form
		check_basic_details
	end

	def test_adding_comment_textfield(name = 'ecf_1')
		# Generate field value
		field_value = self.class.random_string 30

		# Fill out comment form
		insert_basic_details
		@@client.text_field(:name => name).set field_value

		# Submit and verify results
		submit_form
		check_basic_details
		assert (@@client.text.include? field_value),
			'Text field value not reproduced.'
	end

	def test_adding_comment_textarea(name = 'ecf_5')
		# Generate field value
		field_paragraphs = self.class.random_paragraphs [300, 150]
		field_value = field_paragraphs.join "\n\n"

		# Fill out comment form
		insert_basic_details
		@@client.text_field(:name => name).set field_value

		# Submit and verify results
		submit_form
		check_basic_details
		assert field_paragraphs.all?{ |value| @@client.text.include? value },
			'Text area value not reproduced.'
	end

	def test_adding_comment_radio(name = 'ecf_2',
		values = ['Alpha', 'Beta', 'Gamma'])
		# Generate selection
		selection = values[rand(values.length - 1)]

		# Fill out comment form
		insert_basic_details
		@@client.radio(:name => name, :value => selection).set

		# Submit and verify results
		submit_form
		check_basic_details
		assert (@@client.text.include? selection),
			'Radio selection not reproduced.'
	end

	def test_adding_comment_select(name = 'ecf_3',
		values = ['Male', 'Female'])
		# Generate selection
		selection = values[rand(values.length - 1)]

		# Fill out comment form
		insert_basic_details
		@@client.select_list(:name => name).select selection

		# Submit and verify results
		submit_form
		check_basic_details
		assert (@@client.text.include? selection),
			'Selection not reproduced.'
	end

	def test_adding_comment_checkboxes(name = 'ecf_6',
		values = ['Eine', 'Zwei', 'Drei'])
		# Generate selection
		selection = values.select { rand(2) == 0 }

		# Fill out comment form
		insert_basic_details
		selection.each do |value|
			@@client.checkbox(:name => name + '[]', :value => value).set
		end

		# Submit and verify results
		submit_form
		check_basic_details

		selection.each do |value|
			assert (@@client.text.include? value), "'#{value}' not reproduced."
		end

		(values - selection).each do |value|
			assert (@@client.text.include? value),
				"'#{value}' erroneously reproduced."
		end
	end

	def test_adding_comment_image_upload(name = 'ecf_4')
		path = @@config['image_dir']

		# Select an image
		images = Dir[path + '*.{png,jpg}']
		image = images[rand(images.length - 1)]
		image_name = image.sub(path, '')
		image_base_name = image_name.sub(%r{\.(png|jpg)$}, '')
		image_extension = $1

		# Fill out comment form
		insert_basic_details
		@@client.file_field(:name => name).set image

		# Submit and verify results
		submit_form
		check_basic_details
		assert ((URI.encode @@client.html).include? URI.encode image_base_name),
			'Uploaded image not included.'
	end

	private

	def submit_form
		@@client.form(:id => 'commentform').submit
	end

	def insert_basic_details
		@comment_values = self.class.random_paragraphs [400, 400, 200]
		@comment_value = @comment_values.join "\n\n"

		@@client.text_field(:id => 'author').set 'Mr Test'
		@@client.text_field(:id => 'email').set 'test@example.com'
		@@client.text_field(:id => 'url').set 'http://www.example.com'
		@@client.text_field(:id => 'comment').set @comment_value
	end

	def check_basic_details
		assert @comment_values.all?{ |value| @@client.text.include? value },
			'Comment text not reproduced.'
	end

end

