require 'watir-webdriver'
require 'test-unit'
require_relative 'testing'

class TestAdmin < Test::Unit::TestCase

	extend Testing

	class << self

		def startup
			@@config = load_config

			@@browser = @@config['browser']
			@@client ||= Watir::Browser.new @@browser
			@@admin_page_url = @@config['site_url']\
				+ 'wp-admin/edit-comments.php?page=ecf-field-options'

			@@added_fields = []
			@@client.goto @@admin_page_url
			login @@config['user']['username'], @@config['user']['password']

			goto_admin_page
		end

		def shutdown
			@@added_fields.each do |name|
				goto_admin_page
				delete_fields name, false
			end

			@@client.close
		end

		def goto_admin_page
			@@client.goto @@admin_page_url
			disable_confirm
		end

		def disable_confirm
			@@client.execute_script 'window.confirm = function() '\
				'{ return true; }'
		end

		def delete_fields(name, unlist_field = true)
			while fields_by_name(name).first do
				disable_confirm
				fields_by_name(name).first.link(:name => 'delete').click
			end
			@@added_fields.delete name if unlist_field
		end

		def fields_table_rows
			rows = @@client.form(:name => 'ecf-field-table').table.rows.to_a
			rows.first rows.length - 1
		end

		def fields_by_name(name)
			fields_table_rows.select { |row| row.th.text == name }
		end

		private

		def login(username, password)
			@@client.wait_until { @@client.text_field(:id =>
				'user_login').exist? }
			until username == @@client.text_field(:id => 'user_login').value
				@@client.text_field(:id => 'user_login').set username
				sleep 0.1
			end
			@@client.text_field(:id => 'user_pass').set password
			@@client.form(:name => 'loginform').submit
		end
	end

	def setup
		self.class.goto_admin_page
	end

	def test_adding_field_plain
		# Generate random field name
		name = self.class.random_string 8

		# Add a new field
		add_field :name => name

		# Submit and verify results
		submit_form_table_add
		check_table_options self.class.fields_by_name(name).first,
			:name => name

		name
	end

	def test_adding_field
		# Generate random form data
		options = random_options

		# Add a new field
		add_field options

		# Submit and verify results
		submit_form_table_add
		check_table_options self.class.fields_by_name(options[:name]).first,
			options
	end

	def test_adding_field_enter
		# Generate random form data
		options = random_options

		# Add a new field
		add_field options

		# Submit and verify results
		submit_form_table_add_enter
		check_table_options self.class.fields_by_name(options[:name]).first,
			options
	end

	def test_deleting_field
		# Add a new filed to be deleted
		name = test_adding_field_plain

		# Delete fields with that name
		self.class.delete_fields name

		# Check that they are gone
		assert (! @@client.text.include? name),
			'The fields should have been deleted.'
	end

	def test_editing_options
		# Add a new filed to be edited
		original_name = test_adding_field_plain

		# Generate random form data
		options = random_options

		# Edit the details
		edit_field self.class.fields_by_name(original_name).first, options

		# Submit and verify results
		submit_form_options
		check_options options

		# Update data
		@@added_fields.delete original_name
		@@added_fields.push options[:name]
	end

	private

	def add_field(options = {})
		@@added_fields.push options[:name]
		@@client.text_field(:name => 'name[ecf_new]').set options[:name]
		edit_table_field 'ecf_new', options
	end

	def edit_table_field(ref, options = {})
		if options.has_key?(:type) then
			@@client.select_list(:name => "type[#{ref}]").select_value options[:type]
		end
		if options.has_key?(:public) then
			checkbox = @@client.checkbox(:name => "public[#{ref}]")
			checkbox.clear
			checkbox.set options[:public]
		end
		if options.has_key?(:required) then
			checkbox = @@client.checkbox(:name => "required[#{ref}]")
			checkbox.clear
			checkbox.set options[:required]
		end
	end

	def edit_field(row, options = {})
		row.link(:name => 'edit').click
		form = @@client.form(:name => 'ecf_field_options')

		{ :name => /^name/, :order => /^order/,
			:multisite_blogids => /^multisite_blogids/,
			:exclude_pages => /^exclude_pages/, :pages => /^pages/,
			:post_types => /^post_types/ }.each do |key, name|
			if options.has_key?(key) then
				form.text_field(:name => name).set options[key]
			end
		end

		if options.has_key?(:type) then
			form.select_list(:name => /^type/).select_value options[:type]
		end

		{ :public => /^public/, :required => /^required/, :email => /^email/,
			:logged_in_users_only => /^logged_in_users_only/
			}.each do |key, name|
			if options.has_key?(key) then
				checkbox = form.checkbox(:name => name)
				checkbox.clear
				checkbox.set options[key]
			end
		end
	end

	def check_options(options = {})
		form = @@client.form(:name => 'ecf_field_options')

		{ :name => /^name/, :order => /^order/,
			:multisite_blogids => /^multisite_blogids/,
			:exclude_pages => /^exclude_pages/, :pages => /^pages/,
			:post_types => /^post_types/ }.each do |key, name|
			if options.has_key?(key) then
				assert_equal options[key].to_s,
					form.text_field(:name => name).value
			end
		end

		if options.has_key?(:type) then
			assert_equal options[:type],
				form.select_list(:name => /^type/).value,
				'The type should have been updated correctly.'
		end

		{ :public => /^public/, :required => /^required/, :email => /^email/,
			:logged_in_users_only => /^logged_in_users_only/
			}.each do |key, name|
			if options.has_key?(key) then
				assert_equal options[key],
					form.checkbox(:name => name).set?
			end
		end
	end

	def submit_form_table_add
		@@client.button(:value => 'Add').click
	end

	def submit_form_table_add_enter
		@@client.text_field(:name => 'name[ecf_new]').send_keys :enter
	end

	def submit_form_table
		@@client.form(:name => 'ecf-field-table').submit
	end

	def submit_form_options
		@@client.form(:name => 'ecf_field_options').submit
	end

	def check_table_options(row, options = {})
		if options.has_key?(:name) then
			assert row.th.text.include? options[:name]
		end
		if options.has_key?(:type) then
			assert_equal options[:type],
				row.select_list(:name => /^type/).value,
				'The correct field type should be selected.'
		end
		if options.has_key?(:public) then
			assert_equal options[:public],
				row.checkbox(:name => /^public/).set?,
				'Public should be set to the correct value.'
		end
		if options.has_key?(:required) then
			assert_equal options[:required],
				row.checkbox(:name => /^required/).set?,
				'Required should be set to the correct value.'
		end
	end

	def random_options
		types = ['text', 'checkboxes', 'radio', 'select', 'textarea']
		{
			:name => (self.class.random_string 8),
			:type => types[rand(types.length - 1)],
			:public => rand(2) == 0,
			:required => rand(2) == 0,
			:email => rand(2) == 0,
			:logged_in_users_only => rand(2) == 0,
			:order => rand(20),
			:pages => ((1..10).map { rand(10).to_s }.join ', '),
			:exclude_pages => ((1..10).map { rand(10).to_s }.join ', '),
			:multisite_blogids => ((1..10).map { rand(10).to_s }.join ', '),
			:post_types => ((1..10).map { self.class.random_string
				10 }.join ', '),
			:values => ((1..10).map { self.class.random_string 10 }.join ', ')
		}
	end

end
