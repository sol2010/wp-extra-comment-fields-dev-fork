# Various tools for creating and using tests
module Testing
	def load_config(path = 'tests/config.yaml')
		require 'yaml'

		begin
			YAML.load File.open path, 'r'
		rescue Errno::ENOENT => e
			puts "Config file '#{path}' not found"
			exit
		rescue Exception => e
			puts "Could not parse test config: #{e.message}"
			exit
		end
	end

	def random_string(length = 8)
		(0...length).map do
			(('a'..'z').to_a + [' ']*10)[rand 36]
		end.join.gsub(%r{ +}, ' ').gsub(%r{^ | $}, '')
	end

	def random_paragraphs(lengths)
		lengths.map { |length| random_string length }
	end
end