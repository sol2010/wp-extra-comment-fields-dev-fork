<?php
/* An abstract class for providing list based options */
class ECF_Option_Field_Name extends ECF_Option {

	public function get_name() {
		return 'name';
	}

	public function get_full_name() {
		return 'Field Name';
	}

	public function get_description() {
		return 'The name of the field.';
	}

	public function get_default_value() {
		return '';
	}
	
	public function options_form_field( $ref, $field = null ) {
		return sprintf( '<input type="text" class="regular-text" '
				. 'name="%s[%s]" value="%s" />', $this->get_name(), $ref,
				$this->get_value( $field ) );
	}

	public function get_value( $field ) {
		global $ecfdb;

		return $ecfdb->html_string( parent::get_value( $field ) );
	}

	public function priority() {
		return 100;
	}
}
new ECF_Option_Field_Name();
?>