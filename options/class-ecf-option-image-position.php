<?php
require_once ECF_CLASSES_DIR . '/class-ecf-option-radio.php';

/* An abstract class for providing list based options */
class ECF_Option_Image_Position extends ECF_Option_Radio {

	public function __construct() {
		parent::__construct( array( 'image' ) );
	}

	public function get_choices() {
		return array(
			'with-other-fields' => 'With other Extra Comment Fields',
			'left-center' => 'In a seperate vertically centered column '
				. 'to the left of the comment text',
			'float-left-top' => 'Floated to the top left of the comment text',
			'right-center' => 'In a seperate vertically centered column to '
				. 'the right of the comment text',
			'float-right-top' => 'Floated to the top right of the comment '
				. 'text',
		);
	}

	public function get_name() {
		return 'image_positon';
	}

	public function get_full_name() {
		return 'Image Position';
	}

	public function get_description() {
		return 'Where within the comment do you want this image to be '
			. 'displayed?';
	}

	public function get_default_value() {
		return 'with-other-fields';
	}

	public function priority() {
		return 94;
	}
}
new ECF_Option_Image_Position();
