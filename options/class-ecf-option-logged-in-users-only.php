<?php
require_once ECF_CLASSES_DIR . '/class-ecf-option-boolean.php';

class ECF_Option_Logged_In_Users_Only extends ECF_Option_Boolean
	implements ECF_Visibility_Condition {

	public function get_name() {
		return 'logged_in_users_only';
	}

	public function get_full_name() {
		return 'Logged In Users Only';
	}

	public function get_description() {
		return 'Enable to display this field only to users who are logged in.';
	}

	public function get_default_value() {
		return false;
	}

	public function is_satisfied( $field ) {
		// Check if the user needs to be logged in, and if so is
		return ! $this->get_value( $field ) || is_user_logged_in();
	}

	public function priority() {
		return 74;
	}
}
new ECF_Option_Logged_In_Users_Only();
?>