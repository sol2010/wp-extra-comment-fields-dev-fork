<?php
class ECF_Option_Placeholder_Image extends ECF_Option {

	public function __construct() {
		parent::__construct( array( 'image' ) );
	}

	public function get_name() {
		return 'placeholder_image';
	}

	public function get_full_name() {
		return 'Placeholder Image';
	}

	public function get_description() {
		return 'Upload an image to be displayed when a user does not upload '
			. 'an image';
	}

	public function is_value_url( $field ) {
		return filter_var( $this->get_value( $field ), FILTER_VALIDATE_URL )
			!== false;
	}

	private function get_checkbox_prefix() {
		return sprintf( '%s%s', $this->get_name(), '_enabled' );
	}

	private function get_checkbox_name( $ref ) {
		return sprintf( '%s[%s]', $this->get_checkbox_prefix(), $ref );
	}

	public function options_form_field( $ref, $field = null ) {
		$html = '';

		if ( self::is_value_url( $field ) ) {
			$thumbnail_size = ECF_Option::get_option_value( 'display_size',
				$field );
			$url = $this->get_value( $field );

			$html .= ECF_HTML_Formatting::image( $url,
				'ecf-option-placeholder-preview', $thumbnail_size['w'],
				$thumbnail_size['h'], 'Placeholder image preview' );
		}

		$html .= ECF_HTML_Formatting::checkbox_field(
			$this->get_checkbox_name( $ref ), $this->is_value_url( $field ),
			'ecf-option-placeholder-enabled' );
		$html .= ' ';
		$html .= ECF_HTML_Formatting::image_upload_field(
			$this->get_field_name( $ref ), 'ecf-option-placeholder-file' );

		return $html;
	}

	public function options_form_post_single( $ref, $field = null ) {
		$checkbox_prefix = $this->get_checkbox_prefix();
		$name = $this->get_name();
		$field_name = $this->get_field_name( $ref );

		if ( ! ( isset( $_POST[$checkbox_prefix][$ref] )
			&& $_POST[$checkbox_prefix][$ref] ) ) {
			$this->update_value( $field, '' );
		}
		else if ( isset( $_FILES[$name]['name'][$ref] )
			&& ECF_Main::is_image( $_FILES[$name]['name'][$ref],
			$_FILES[$name]['type'][$ref] ) ) {
			$upload = wp_upload_bits( $_FILES[$name]['name'][$ref], null,
				file_get_contents( $_FILES[$name]['tmp_name'][$ref] ) );
			$this->update_value( $field, $upload['url'] );
		}
	}

	public function get_default_value() {
		return '';
	}

	public function priority() {
		return 93;
	}
}
new ECF_Option_Placeholder_Image();
