<?php
require_once ECF_CLASSES_DIR . '/class-ecf-option-select.php';

class ECF_Option_Field_Type extends ECF_Option_Select {

	public function get_name() {
		return 'type';
	}

	public function get_full_name() {
		return 'Field Type';
	}

	public function get_description() {
		return 'The type of the field.';
	}

	public function get_default_value() {
		return 'text';
	}

	public function get_choices() {
		$choices = array();

		foreach ( ECF_Field_Type::get_types() as $name => $type ) {
			$choices[$name] = $type->get_description();
		}

		return $choices;
	}

	public function priority() {
		return 98;
	}
}
new ECF_Option_Field_Type();
