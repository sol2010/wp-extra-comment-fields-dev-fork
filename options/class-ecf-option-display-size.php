<?php
require_once ECF_CLASSES_DIR . '/class-ecf-option-dimensions.php';

class ECF_Option_Display_Size extends ECF_Option_Dimensions {

	public function __construct() {
		parent::__construct( array( 'image' ) );
	}

	public function get_name() {
		return 'display_size';
	}

	public function get_full_name() {
		return 'Display Size';
	}

	public function get_description() {
		return 'The size at which the image will be displayed (whilst '
			. 'avoiding distorting the image).';
	}

	public function get_default_value() {
		return array( 'w' => 400, 'h' => 400 );
	}

	public function priority() {
		return 95;
	}
}
new ECF_Option_Display_Size();