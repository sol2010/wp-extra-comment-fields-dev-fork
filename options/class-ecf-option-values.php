<?php
require_once ECF_CLASSES_DIR . '/class-ecf-option-list.php';

class ECF_Option_Values extends ECF_Option_List {

	public function __construct() {
		parent::__construct( array( 'select', 'radio', 'checkboxes' ) );
	}

	public function get_name() {
		return 'values';
	}

	public function get_full_name() {
		return 'Values';
	}

	public function get_description() {
		return 'The options which may be selected with the field.';
	}

	public function get_default_value() {
		return '';
	}

	public function priority() {
		return 95;
	}
}
new ECF_Option_Values();
?>