<?php
class ECF_Image_Upload extends ECF_Field_Type {
	protected $name = 'image';

	private static final function thumbnail( $url, $h='null',
			$w='null', $zc=3 ) {
		return ECF_HTML_Formatting::thumbnail( $url, $w, $h, $zc );
	}

	public function form_field( $ref, $field ) {
		global $ecfdb;
		?>
		<label class='ecf-form-field-title' for="<?php echo $ref ?>">
			<?php echo $ecfdb->html_string( $field->name ); ?>
		</label>
		<input type="file" class='ecf-form-field-input'
			name="<?php echo $ref ?>"
			id="<?php echo $ref ?>" />
		<?php
	}

	public function display_field( $id, $name, $value ) {
		global $ecfdb;

		$field = $ecfdb->get_field( $id );

		$thumbnail_size = ECF_Option::get_option_value( 'display_size',
			$field );
		$url = self::thumbnail( $value, $thumbnail_size['h'],
			$thumbnail_size['w'] );
		$larger = self::thumbnail( $value, 600, 800 );

		$classes = array();
		$classes[] = "ecf-field";
		$classes[] = "ecf-field-$id";
		if ( ECF_Option::is_option( 'image_positon' ) ) {
			$classes[] = 'ecf-image-position';
			$classes[] = 'ecf-image-position-'
				. ECF_Option::get_option_value( 'image_positon', $field );
		}
		$class_list = implode( ' ',  $classes );

		return "<figure class='$class_list'>\n"
			. "<div class='ecf-response'><a href='$larger'>"
			. "<img src='$url' class='ecf-image'"
			. "alt='$name' /></a></div>"
			. "\t<figcaption class='ecf-question'>$name</figcaption>"
			. "</figure>\n";
	}

	public function display_plaintext_field( $name, $value ) {
		$url = self::thumbnail( $value, 400, 400 );
		return "$name:\n$url";
	}

	public function get_description() {
		return "Image upload field";
	}
}

new ECF_Image_Upload();
?>