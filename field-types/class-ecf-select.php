<?php
/* Selection form */
class ECF_Select extends ECF_Field_Type {
	protected $name = 'select';

	public function form_field( $ref, $field ) {
		global $ecfdb;
		$name = $ecfdb->html_string( $field->name );
		echo "<label class='ecf-form-field-title' for='$name'>$name</label>\n";
		$values = preg_split( '/, ?/', $field->values );
		echo "<select class='ecf-form-field-input' name='$ref'>\n";
		foreach ( $values as $value ) {
			echo "\t<option value='$value'>$value</option>\n";
		}
		echo "</select>\n";
	}

	public function get_description() {
		return "Dropdown selection field";
	}
}

new ECF_Select();
?>
