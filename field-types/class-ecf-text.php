<?php
/* Text form */
class ECF_Text extends ECF_Field_Type {
	protected $name = 'text';

	public function form_field( $ref, $field ) {
		global $ecfdb;
		$name = $ecfdb->html_string( $field->name );
		echo "<p class='comment-form-author'>\n";
		echo "<label class='ecf-form-field-title' for='$ref'>$name</label>\n";
		$req = ECF_Option::get_option( 'required' )->get_value( $field );
		$required = $req ? 'required ' : '';
		$aria_required = $field->required ? 'true' : 'false';
		if ( $req )
			echo "<span class='required'>*</span>";
		echo "<input type='text' name='$ref' class='ecf-form-field-input' "
			. "id='$ref' size='25' $required"
			. "aria-required='$required' />\n";
		echo "</p>";
	}

	public function get_description() {
		return "Text field";
	}
}

new ECF_Text();
?>
