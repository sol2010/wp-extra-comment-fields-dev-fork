<?php
/*
Plugin Name: WP Extra Comment Fields
Plugin URI: http://www.solaceten.info
Description: Add extra comment form fields (dropdowns, radios, text) to the standard WordPress comment form and make it more useful. IMPORTANT: see the attached documentation for instructions. Visit <a href="http://www.solaceten.info" target="_blank">http://www.solaceten.info</a> for further information.
Version: 2.24
Author: Sol
Author URI: http://www.solaceten.info
*/

define( 'ECF_VERSION', '2.24' );
define( 'ECF_ROOT_FILE', __FILE__ );
define( 'ECF_ROOT_DIR', dirname( __FILE__ ) );
define( 'ECF_CLASSES_DIR', ECF_ROOT_DIR . '/classes' );
define( 'ECF_INTERFACES_DIR', ECF_ROOT_DIR . '/interfaces' );
define( 'ECF_OPTIONS_DIR', ECF_ROOT_DIR . '/options' );
define( 'ECF_FIELD_TYPES_DIR', ECF_ROOT_DIR . '/field-types' );
define( 'ECF_JS_PATH', '/js' );
define( 'ECF_JS_DIR', ECF_ROOT_DIR . ECF_JS_PATH );
define( 'ECF_CSS_PATH', '/css' );
define( 'ECF_CSS_DIR', ECF_ROOT_DIR . ECF_CSS_PATH );
define( 'ECF_LIB_PATH', '/lib' );
define( 'ECF_LIB_DIR', ECF_ROOT_DIR . ECF_LIB_PATH );

function ecf_plugin_data( $name = null ) {
	$data = array(
		'version' => ECF_VERSION,
		'website' => 'http://www.solaceten.info/',
		'edition' => 'develop',
		'authors' => array( 'Thomas Wright', 'Solace' ),
		'links' => array(
			'Support' => 'https://www.solaceten.info/clientarea/submitticket.php',
			'Client Area' => 'https://www.solaceten.info/clientarea/',
			'Our other plugins' => 'http://www.solaceten.info/clientarea/cart.php',
			'Upgrade Your Plugin to unlock additional features'
				=> 'https://www.solaceten.info/clientarea/clientarea.php?action=products'
		),
		'update_url' => 'https://www.solaceten.info/clientarea/clientarea.php?action=products'
	);
	return is_null( $name ) ? $data : $data[$name];
}

function ecf_js_dump( $msg ) {
	echo '<script type="text/javascript">alert(';
	echo json_encode( var_export( $msg, true ) );
	echo ');</script>';
}

function ecf_file_dump( $msg ) {
	$filename = '/debug-messages';
	$f = fopen( ECF_ROOT_DIR . "/$filename", 'a' );
	fwrite( $f, var_export( $msg, true ) . "\n" );
	fclose( $f );
}

require_once ECF_CLASSES_DIR . '/class-ecf-html-formatting.php';
require_once ECF_INTERFACES_DIR . '/interface-ecf-unavailable.php';
require_once ECF_INTERFACES_DIR . '/interface-ecf-visibility-condition.php';
require_once ECF_CLASSES_DIR . '/class-ecf-option.php';
require_once ECF_CLASSES_DIR . '/class-ecf-field-type.php';

// Include all custom field types
foreach ( glob( ECF_ROOT_DIR . '/field-types/*.php' )
	as $filename ) {
	include_once $filename;
}
// Include all options
foreach ( glob( ECF_ROOT_DIR . '/options/*.php' )
	as $filename ) {
	include_once $filename;
}

require_once ECF_CLASSES_DIR . '/class-ecf-settings-global.php';
$GLOBALS['ecf_settings_global'] = new ECF_Settings_Global();
require_once ECF_CLASSES_DIR . '/class-ecf-plugin-api.php';
$GLOBALS['ecf_plugin_API'] = new ECF_Plugin_API();
require_once ECF_CLASSES_DIR . '/class-ecf-db.php';
$GLOBALS['ecfdb'] = new ECF_DB();
require_once ECF_CLASSES_DIR . '/class-ecf-main.php';
new ECF_Main();
